// tests
//import { read } from '../src/lisp.js'
import {read} from '../src/lisp.js';


function myTest(a,b) {
    if (a.type != b.type) {
        console.log('\x1b[31m%s\x1b[0m',"Erreur de type")
    }
    if (a.name != b.name) {
        console.log('\x1b[31m%s\x1b[0m',"Erreur de name")
    }
    if (a.val != b.val) {
        console.log('\x1b[31m%s\x1b[0m',"Erreur de val")
    }
    console.log('\x1b[32m%s\x1b[0m', 'Good')
}
var a=1
myTest(read("a"), { type: "val", name: "a" })
myTest(read("1"), { type: "num", val: "1" })

console.log("helloWorld")
